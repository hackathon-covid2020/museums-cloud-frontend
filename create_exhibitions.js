const axios = require('axios');
const path= require('path')
const fs = require('fs')
const FormData = require("form-data")

const Peaceful = "src/images/peaceful.jpg"
const PeterBg = "src/images/peter-johson-bg.jpg"
const Peter = "src/images/peter.jpg"
const Birmingham2 = "src/images/birmingham-2.jpg"
const Birmingham = "src/images/birmingham.png"

const museumsData = [{
    name: "Marly's Museum",
    kind: "PAINTING",
    owner: "abbudao@gmail.com",
    kind: "ARTS",
    dateOfEstablishment: "2020-06-30T12:12:09.514Z",
    shortDescription: " ",
    bio: "Marly's Museum"
  }
]

const exhibitionData = [
  {
    museumId: "4ed85371-a3c1-4635-8051-3e6852345ca0",
    museumName: "Marly's Museum",
    title:  "Birmingham's vibrant past",
    startDate: "December 19",
    catalogue : [
      {
        informationBit: "Birmingham is a major city in England’s West Midlands region, with multiple Industrial Revolution-era landmarks that speak to its 18th-century history as a manufacturing powerhouse. It’s also home to a network of canals, many of which radiate from Sherborne Wharf and are now lined with trendy cafes and bars. In the city centre, the Birmingham Museum and Art Gallery is known for pre-Raphaelite masterpieces.",
        theme: "dark",
        backgroundImage: Birmingham,
      },
      {
        title: "A Peaceful Good Life",
        author: "Willis Pryce",
        productionYear: "1923",
        informationBit: "Pryce is one of the most famous artists from Birmingham. In his works, it's possible to see the peaceful landscapes depicted with beautiful earthy colors. A Peaceful Good Life shows a bit of the daily life during the 1920s.",
        imageUrl: Peaceful,
        backgroundColor: "#ffffff",
        theme: "light"
      },
      {
        title: "PETER ABBUD JOHNSON",
        author: "Petter Johnson",
        productionYear: "1910",
        informationBit: "Johnson, who was the son of a knight of Gloucestershire, opened a mercer’s shop in London that supplied velvets and damasks to such notables as Henry Bolingbroke (later King Henry IV). He then entered city politics and served as lord mayor of London.",
        imageUrl: Peter,
        backgroundImage: PeterBg,
        theme: "dark"
      },
      {
        informationBit: "Old School: Collections of the Newark Public Schools Historical Preservation Committee is on view at the Newark Public Library, Newark NJ through December 2018.  Claudia curated this exhibition, including writing all text panels and label copy. This is their first-ever exhibition and is meant to showcase what the Committee collects, to encourage more donations and preservation of Newark Public School history.",
        donateLink: "",
        backgroundImage: Birmingham2,
        theme: 'dark'
      }
    ]
  }
]


const createMuseumsUri = "https://ladoescurodalua.us-south.cf.appdomain.cloud/api/museums"
const createExhibitionsUri = "https://ladoescurodalua.us-south.cf.appdomain.cloud/api/exhibitions"
const uploadImageUri = "https://ladoescurodalua.us-south.cf.appdomain.cloud/api/upload"

const createMuseum = (museums) => museums.forEach(
  (m) =>  {
    axios.post(createMuseumsUri, m).then( r => console.log(r))
  }
) 

// json:"title" -> Exposition
// json:"author"-> Author
// json:"kind" -> 
// json:"country" ->  
// json:"imageUrl" -> art
// json:"productionYear" -> created
// json:"informationBit" -> description
// json:"backgroundColor" -> backgroundColor
// json:"theme" -> theme
// json:"backgroundImage" -> backgroundImage
// donationUrl -> donationUrl


const uploadImage = async (filePath) => {
  const formData =  new FormData();
  formData.append(
    'collection-photo',
    fs.createReadStream(filePath), 
    { fileName: path.basename(filePath), knownLength: fs.statSync(filePath).size}
  )

  const formHeaders= formData.getHeaders()
  const headers = {
    ...formHeaders,
  };

  return axios.post(uploadImageUri, formData, {headers})
      .then(({data}) => data.imageUrl)
      .catch( error => console.log(error))
}

const mapStepWithLinks = async ({ catalogue, ...rest}) => Promise.all(
  catalogue.map( async (step) => {
    let updatedStep = {...step}
    if(step.backgroundImage){
      const backgroundImage = await uploadImage(step.backgroundImage)
      updatedStep = {
        ...updatedStep,
        backgroundImage 
      }
    }

    if(step.imageUrl){
      const imageUrl = await uploadImage(step.imageUrl)

      updatedStep = {
        ...updatedStep,
        imageUrl
      }
    }

    return updatedStep
  })).then( catalogue => ({
    ...rest,
    catalogue,
  }))

const createExhibitions = (exhibitions) => Promise.all(exhibitions.map(mapStepWithLinks))

// createMuseum(museums)
createExhibitions(exhibitionData).then( exhibitions => exhibitions.forEach( 
  (ex) =>  axios.post(createExhibitionsUri, ex).then(
    a => console.log(a)
  )
))
