import * as React from "react"

const PlusIcon = (props) =>  (
  <svg width="1em" height="1em" viewBox="0 0 42 42" {...props}>
    <g data-name="Group 44" fill="none" stroke={props.theme === "dark" ?  "#ffffff" : "#24221f"} strokeWidth={2}>
      <g data-name="Ellipse 8">
        <circle cx={21} cy={21} r={21} stroke="none" />
        <circle cx={21} cy={21} r={20} />
      </g>
      <g data-name="Group 43" strokeLinecap="round">
        <path data-name="Line 1" d="M21 11v20" />
        <path data-name="Line 2" d="M31 21H11" />
      </g>
    </g>
  </svg>
);

export default PlusIcon;
