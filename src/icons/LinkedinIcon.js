import * as React from "react"

function SvgComponent(props) {
  return (
    <svg
      id="prefix__linkedin"
      width={24}
      height={24}
      viewBox="0 0 24 24"
      {...props}
    >
      <path
        id="prefix__Path_10"
        d="M23.994 24H24v-8.8c0-4.306-.927-7.623-5.961-7.623a5.226 5.226 0 00-4.707 2.587h-.07V7.976H8.489V24h4.97v-7.935c0-2.089.4-4.109 2.983-4.109 2.549 0 2.587 2.384 2.587 4.243V24z"
        fill="#fff"
        data-name="Path 10"
      />
      <path
        id="prefix__Path_11"
        d="M.4 7.977h4.972V24H.4z"
        fill="#fff"
        data-name="Path 11"
      />
      <path
        id="prefix__Path_12"
        d="M2.882 0a2.9 2.9 0 102.882 2.882A2.883 2.883 0 002.882 0z"
        fill="#fff"
        data-name="Path 12"
      />
    </svg>
  )
}

export default SvgComponent