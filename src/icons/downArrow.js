import * as React from "react"
import { motion } from "framer-motion";

const DownArrowIcon = (props) => (
  <motion.svg width="1em" height="1em" viewBox="0 0 74.902 17.82" {...props}>
    <motion.path
      fill="none"
      stroke={props.theme === "dark" ?  "#ffffff" : "#24221f"}
      strokeLinecap="round"
      strokeWidth={4}
      d="M1.948 1.954l35.894 14.25 35.106-14.25"
    />
  </motion.svg>
)


export default DownArrowIcon;
