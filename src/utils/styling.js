import { css } from "styled-components"
import { media } from "./media"

const container = css`
	padding-left: calc(50vw - 640px);
	padding-right: calc(50vw - 640px);
	box-sizing: border-box;

	${media.tablet`
	padding-left: calc(50vw - 377px);
	padding-right:calc(50vw - 377px);
	`}

	${media.mobile`
	padding-left: calc(50vw - 167.5px);
	padding-right: calc(50vw - 167.5px);
	`}
`

export { container }
