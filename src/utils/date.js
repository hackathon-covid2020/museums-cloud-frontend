import { format } from "date-fns";

const formatDate = (timestamp) => {
  const date = new Date(timestamp)
  const month = format(date, "MMM")
  const year = format(date, "yy")
  return `${month}. ${year}`
}

const formatDateLong = (timestamp) => {
  const date = new Date(timestamp)
  console.log(date)
  const month = format(date, "MMMM")
  const year = format(date, "yy")
  return `${month} ${year}`
}

export { formatDate, formatDateLong }
