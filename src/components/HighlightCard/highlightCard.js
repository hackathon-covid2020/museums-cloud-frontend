import React from 'react';
import { Link as BaseLink } from "gatsby"
import styled from "styled-components";
import PropTypes from "prop-types";
import Wave from "images/wave.jpg"
import { media, formatDate } from "utils";

const DateText = styled.div`
  text-transform: uppercase;
  font-family: "Barlow Condensed";
  font-stretch: condensed;
  color: #656360;
  font-size: 20px;
  line-height: 37px;
  ${media.tablet`
  font-size: 18px;
  line-height: 35px;
  `}
  ${media.mobile`
  font-size: 17px;
  line-height: 34px;
  `}
`;

const Link = styled(BaseLink)`
  text-decoration: none;
`

const MuseumText = styled.div`
  font-family: "Barlow Condensed";
  font-size: 24px;
  font-stretch: condensed;
  line-height: 29px;
  color: #9d9b9a;
  ${media.tablet`
  font-size: 22px;
  line-height: 26px;
  `}
  ${media.mobile`
  font-size: 21px;
  line-height: 25px;
  `}
`

const ExpositionText = styled.div`
  font-family: "Barlow Condensed";
  font-size: 32px;
  font-weight: 500;
  font-stretch: condensed;
  line-height: 38px;
  color: #24221f;
  ${media.tablet`
  line-height: 36px;
  font-size: 30px;
  `}
  ${media.mobile`
  font-size: 28px;
  line-height: 34px;
  `}
`

const CardImageContainer = styled.div`
  width: 400px;
  height: 240px;

  ${media.tablet`
  width: 359px;
  height: 215px;
  `}
  ${media.mobile`
  width: 335px;
  height: 201px;
  `}
  overflow: hidden;
`;

const CardImage = styled.div`
  background-image: url(${ ({src}) => src});
  background-position: center;
  background-size: cover;
  width: 100%;
  height: 100%;
  transition: all .5s;
`



const ExpoContainer = styled.div`
  display:flex;
  flex-direction: column;
  align-items: flex-start;
  text-overflow: ellipsis;

  & > ${DateText} {
    margin-right: 24px;
  };
  & > ${ExpositionText} {
  margin-bottom: 8px;
  }

  ${media.tablet`
    & > ${ExpositionText} {
    margin-bottom: 7px;
    }
    & > ${DateText} {
      margin-right: 18px;
    };
  `}
  ${media.mobile`
    & > ${DateText} {
        margin-right: 17px;
    };
  `}
`;

const CardContent = styled.div`
  display:flex;
  padding: 24px;

  & > ${MuseumText} {
  margin-left: 79px;
  };

  ${media.tablet`
  padding: 22px;
  & > ${MuseumText} {
  margin-left: 75px;
  };
  `}
  ${media.mobile`
  padding: 21px;
  & > ${MuseumText} {
  margin-left: 73px;
  };
  `}

  & > ${DateText} {
    margin-right: 24px;
  };
  margin-bottom: 8px;

  ${media.tablet`
    margin-bottom: 7px;
    & > ${DateText} {
      margin-right: 18px;
    };
  `}
  ${media.mobile`
    margin-bottom: 7px;
    & > ${DateText} {
        margin-right: 17px;
    };
  `}
`

const CardContainer = styled.div`
cursor: pointer;
background-color: #eeedeb;
width: 400px;
height: 363px;
transition-duration: 0.5s;
transition-property: color, background-color;
${media.tablet`
  width: 359px;
  height: 328px;
`}
${media.mobile`
  width: 335px;
  height: 309px;
`}
&:hover {
vertical-align: middle;
transform: perspective(1px) translateZ(0);
box-shadow: 0 0 1px rgba(0, 0, 0, 0);
overflow: hidden;
background-color:#24221f;

& > ${CardImageContainer} > ${CardImage}  {
  transform: scale(1.2);
  }
& > ${CardContent} > ${DateText} {
  color: #eeedeb;;
}
& > ${CardContent} > ${ExpoContainer} > ${MuseumText} {
  color: #9d9b9a;
}
& > ${CardContent} > ${ExpoContainer} > ${ExpositionText} {
  color:white;
}
}
`;


const HighlightCard = ({ date, image, exposition, museum, link }) => (
  <Link
    to={link}
  >
    <CardContainer>
      <CardImageContainer>
        <CardImage src={image} />
      </CardImageContainer>
      <CardContent>
        <DateText>{formatDate(date)}</DateText>
        <ExpoContainer>
          <ExpositionText>{exposition}</ExpositionText>
          <MuseumText>{museum}</MuseumText>
        </ExpoContainer>
      </CardContent>
    </CardContainer>
  </Link>
);

HighlightCard.defaultProps = {
  date: Date.now(),
  image: Wave,
  exposition: "Abbud's Favorite",
  museum: "Abbuseum",
}

HighlightCard.propTypes = {
  date: PropTypes.number,
  image: PropTypes.string,
  exposition: PropTypes.string,
  museum: PropTypes.string,
}

export default HighlightCard;
