import React from "react"
import Logo from "icons/logo"
import styled from "styled-components"
import { motion } from "framer-motion"
import { Link } from "gatsby"
import { media, container } from "utils";

const footerLinks = [
	{
		label: 'Museums list',
		link: '/'
	},
	{
		label: 'Make a donation',
		link: '/'
	},
	{
		label: 'Share to social',
		link: '/'
	}
]
const FooterNavItems = () => footerLinks.map(({ label, link }, index) => <FooterNavItem key={index} to={link}>{label}</FooterNavItem>)

const StyledFooter = styled(motion.footer)`
	padding: 34px 0;
	${container}
	width: 100%;
	background-color: #24221F;
	display: flex;
	justify-content: space-between;
	align-items: center;

	${media.tablet`
	display: block;
	text-align: center;
	`}
`

const FooterNavItem = styled(Link)`
	text-decoration: none;
	text-transform: uppercase;
	color: #fff;
	font-size: 32px;
	font-weight: 500;
	transition: color .2s ease-out;

	&:hover{
		color: #9d9b9a;
	}
`

const FooterNav = styled(motion.nav)`
	& > ${FooterNavItem}{
		margin-right: 80px;
	}

	${media.tablet`
	display: flex;
	justify-content: space-between;
	margin-bottom: 45px;

	& > ${FooterNavItem}{
		margin-right: 0px;
	}
	`}

	${media.mobile`
	flex-flow: column;
	margin-bottom: 7px;

	& > ${FooterNavItem}{
		margin-bottom: 35px;
	}
	`}
`

const StyledLogo = styled(Logo)`
	width: 80px;
	height: 50px;
	
	& path{
		fill: #9d9b9a;
	}
`

const Footer = () => (
	<StyledFooter>
		<FooterNav>
			<FooterNavItems items={footerLinks}/>
		</FooterNav>
		<Link to="/">
			<StyledLogo />
		</Link>
	</StyledFooter>
)

export default Footer;
