import React from "react"
import styled from "styled-components";
import { motion } from "framer-motion";

import { media } from "utils";
import logo from "images/logo.svg"

import heroBg1 from "images/Johan.jpg"
import heroBg2 from "images/UFSCar.png"
import heroBg3 from "images/carroca.jpg"

const heroBgOptions = [
		{
			img: heroBg1,
			author: 'Johann Zoffany'
		},
		{
			img: heroBg2,
			author: 'UFScar University'
		},
		{
			img: heroBg3,
			author: 'John North'
		}
	]

const Hero = styled.div`
	min-height: 75vh;
	display: flex;
	flex-flow: column;
	align-items: center;
	justify-content: center;
	text-transform: uppercase;
	color: #fff;
	position: relative;
	overflow: hidden;
`

const HeroBlackBackground = styled.div`
	position: fixed;
	width: 100%;
	height: 100%;
	left: 0;
	top: 0;
	z-index: -1;
	background: #000;
`

const HeroBackgroundImage = styled(motion.img)`
	position: fixed;
	width: 100%;
	height: 100%;
	left: 0;
	top: 0;
	z-index: -1;
	filter: blur(5px) brightness(0.5);
	transform: scale(1.05);
	object-fit: cover;
	object-position: center;
`

const HeroText = styled(motion.p)`
	font-size: 80px;
	font-weight: 500;
	text-align: center;
	max-width: 920px;

	${media.tablet`
	font-size: 60px;
	max-width: 690px;
	`}
	${media.mobile`
	max-width: 336px;
	font-size: 42px;
	margin-bottom: 70px;
	`}
`

const HeroImageMeta = styled.div`
	position: absolute;
	top: 51px;
	right: 80px;
	text-transform: capitalize;
	font-size: 24px;
	font-weight: 300;

	${media.tablet`
		font-size: 23px;
		top: 58px;
		right: 40px;
	`}
	${media.mobile`
		position: static;
		font-size: 21px;
		text-align: center;
	`}
`

const HeroLogo = styled.img`
	position: absolute;
	top: 40px;
	left: 80px;

	${media.tablet`
		left: 40px;
		top: 47px;
	`}
	${media.mobile`
		top: 28px;
		left: 50%;
		transform: translateX(-50%);
	`}
`

const HomeHero = () => {
	const [heroBg, setHeroBg] = React.useState({
	})

	React.useEffect(
		() =>	setHeroBg(heroBgOptions[Math.floor(Math.random()*heroBgOptions.length)]),
		[]
	)

	return (
		<Hero>
			<HeroBlackBackground />
			<HeroBackgroundImage
				initial={{ filter: 'blur(0) brightness(1)' }}
				animate={{ filter: 'blur(5px) brightness(0.5)' }}
				transition={{ ease: "easeOut", duration: 1, delay: 2 }}
				src={heroBg.img}
			/>
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1 }}
				transition={{ ease: "linear", duration: 1, delay: 3 }}
			>
				<HeroLogo src={logo}/>
				<HeroText>
					Explore museums and exhibits<br/>around the world
				</HeroText>
				<HeroImageMeta>
					{heroBg.author}
				</HeroImageMeta>
			</motion.div>
		</Hero>
	)
}

export default HomeHero
