import React from "react";
import PageScroller from "react-page-scroller";

import { OpeningStep, AlignedStep, ConclusionStep } from "components/Steps";
import Stepper from "components/Stepper"

const mapStepTypes = (currentStep, length, handleArrowClick) => ( (expo, i) => {
  const active = currentStep === i ;
  const isLast = currentStep === length - 1;

  const commonProps = {
    handleArrowClick,
    isLast,
    active,
    number:i,
    key:i,
  }

  switch (expo.type) {
    case 'opening':
      return <OpeningStep  {...commonProps} {...expo} />

    case 'leftAlignedPiece':
    case 'rightAlignedPiece':
      return <AlignedStep right={expo.type === "rightAlignedPiece"} {...commonProps} {...expo} />

    case 'conclusion':
      return <ConclusionStep {...commonProps} {...expo} />

    default:
      return null
  }
})

const Exposition = ({ steps }) => {
  const [currentStep, setCurrentStep] = React.useState(0)

  const handlePageChange = n => setCurrentStep(n);

  const handleArrowClick = () => handlePageChange(currentStep + 1);

  return (
    <>
      <Stepper active={currentStep} number={steps.length} goToStep={handlePageChange} />
      <PageScroller
        pageOnChange={handlePageChange}
        containerWidth="100vw"
        containerHeight="100vh"
        customPageNumber={currentStep}
      >
        {
            steps.map(mapStepTypes(currentStep, steps.length, handleArrowClick))
        }
      </PageScroller>
    </>
  )
  
};

export default Exposition;
