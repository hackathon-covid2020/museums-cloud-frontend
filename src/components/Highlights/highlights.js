import React, { useMemo } from 'react';
import styled from 'styled-components';
import { container, media } from "utils";

import HighlightCard from "components/HighlightCard";

const HighlightHeader = styled.h2`
  font-family: "Barlow Condensed";
  font-size: 64px;
  font-weight: 500;
  font-stretch: condensed;
  line-height: 77px;
  color: #24221f;

  ${media.tablet`
  font-size: 45px;
  `}

  ${media.mobile`
  font-size: 35px;
  `}
`

const HighlightContainer = styled.div`
${container}
background-color: #fff;
padding-top: 72px;
padding-bottom: 294px;

& > ${HighlightHeader} {
  margin-bottom: 64px;
}

${media.tablet`
padding-bottom: 242px;
& > ${HighlightHeader} {
  margin-bottom: 45px;
}
`}

${media.mobile`
padding-bottom: 147px;
& > ${HighlightHeader} {
  margin-bottom: 42px;
}
`}
`;

const CardGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  gap: 40px;
  ${media.tablet`
  grid-template-columns: 1fr 1fr;
  gap: 36px;
  `}
  ${media.mobile`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center
  `}
`

const mapHighlightCards =  ({ date, image, exposition, museum, link }) => (
  <HighlightCard  
    link={link}
    key={`${exposition}${museum}`}
    date={date}
    image={image}
    exposition={exposition}
    museum={museum}
  />
);

const Highlights = ({highlights}) => {
  const cards = useMemo( () => highlights.map(mapHighlightCards), [highlights]);
  return (
    <HighlightContainer>
      <HighlightHeader>Highlights</HighlightHeader>
      <CardGrid>
        {cards}
      </CardGrid>
    </HighlightContainer>
  )
};

export default Highlights;
