import React, { useState } from 'react';
import styled from 'styled-components';
import { motion } from "framer-motion";
import { media } from "utils";
import Plus from "icons/plus";

import BaseStep from './baseStoryStep';

const PlusIcon = styled(Plus)`
display: none;
 ${media.mobile`
 ${ ({ hideText }) => hideText && "display:block"}
 `}
 width: 42px;
 height: 42px;
 cursor: pointer;
 z-index: 40;
`

const Title = styled(motion.h1)`
  text-transform: uppercase;
  font-size: 48px;
  font-weight: 500;
  font-stretch: condensed;
  line-height: 58px;
  text-align: left;
  color: ${ ({ theme }) => theme !== "dark" ? "#24221f" : "#ffffff"};
  width: 100%;
  ${media.tablet`
  text-align: center;
  font-size: 45px;
  line-height: 1.2;
  `}
  ${media.mobile`
  font-size: 35px;
  `}
`;

const Description = styled(motion.p)`
  font-family: "Barlow Condensed";
  font-size: 32px;
  font-stretch: condensed;
  line-height: 38px;
  text-align: left;
  color: ${ ({ theme }) => theme !== "dark" ? "#24221f" : "#ffffff"};
  width: 100%;
  ${media.tablet`
  text-align: center;
  font-size: 30px;
  line-height: 1.2;
  margin-bottom: 60px;
  `}
  ${media.mobile`
  font-size: 21px;
   ${ ({ hideText }) => hideText && "display: none;"}
  `}
`;

const ArtBox = styled.div`
  width: 576px;
  height: 640px;
  display: flex;
  align-items: center;
  justify-content: center;
  ${media.tablet`
  width: 670px;
  height: 420px;
  `}
  ${media.mobile`
  width: 335px;
  height: 390px;
  ${({ hideText }) => !hideText && `
  width: 309px;
  height: 168px;
  `}
  `}

`

const Art = styled(motion.img)`
  max-width: 100%;
  max-height: 100%;
  box-shadow: 0 12px 24px 0 rgba(36, 34, 31, 0.5);
`

const PieceInfo = styled.p`
  width:100%;
  font-size: 32px;
  font-stretch: condensed;
  font-family: "Barlow Condensed";
  line-height: 38px;
  text-align: left;
  color: ${ ({ theme }) => theme !== "dark" ? "#24221f" : "#ffffff"};
  ${media.tablet`
  text-align: center;
  font-size: 30px;
  line-height: 1.2;
  `}
  ${media.mobile`
  font-size: 24px;
  `}
`

const IntroContainer = styled(BaseStep)`
flex-direction: row;
${media.tablet`
flex-direction: column;
`}
align-items: center;
padding-right: 80px;
padding-left: 160px;
box-sizing: border-box;
${({ right }) => right && "flex-direction: row-reverse;" }
${media.tablet`
  padding-left: 80px;
`}
& > ${ArtBox} {
  ${ ({ right }) => right ?  "margin-left" :  "margin-right"}: 80px;
  ${media.tablet`
    margin: 11vh 0 4vh 0;
  `}
}
`

const TextContainer = styled.div`
display: flex;
align-items: center;
justify-content: center;
flex-direction: column;
width: 576px;
height: 640px;
${media.tablet`
width: 670px;
justify-content: flex-start;
`}
${media.mobile`
width: 309px;
padding: 0px 19.5px;
`}
& > ${Title} {
margin-bottom: 1vh;
}
& > ${PieceInfo} {
margin-bottom: 5vh;
}
`

const IntroductoryStep = ({ art, title, description, active, year, author, right, theme, ...props }) => {
  const [hideText, setHideText] = useState(true)

  return (
    <IntroContainer right={right} active={active} theme={theme} {...props} >
      <ArtBox hideText={hideText}>
        <Art src={art}/>
      </ArtBox>
      <TextContainer>
        <Title theme={theme}>
          {title}
        </Title>
        <PieceInfo theme={theme}>
          {`${author}, ${year}`}
        </PieceInfo>
        <PlusIcon hideText={hideText} onClick={() => setHideText(false)} theme={theme} /> 
        <Description theme={theme} hideText={hideText}>
          {description}
        </Description>
      </TextContainer>
    </IntroContainer>
  )
};

export default IntroductoryStep;
