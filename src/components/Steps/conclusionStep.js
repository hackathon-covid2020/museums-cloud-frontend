import React from 'react';
import { Link } from 'gatsby';
import styled from 'styled-components';
import { media, container } from "utils";
import BaseStep from './baseStoryStep';
import WhatsappIcon from "icons/WhatsappIcon"
import FacebookIcon from "icons/FacebookIcon"
import TwitterIcon from "icons/TwitterIcon"
import LinkedinIcon from "icons/LinkedinIcon"
import { WhatsappShareButton, FacebookShareButton, TwitterShareButton, LinkedinShareButton } from "react-share";

const Text = styled.p`
  font-size: 2rem;
  max-width: 70%;
  text-align: center;
  line-height: 1.1875;

  ${media.tablet`
  font-size: 30px;
  line-height: 1.2;
  max-width: 88%;
  `}

  ${media.mobile`
  font-size: 21px;
  line-height: 1.2;
  max-width: 88%;
  `}
`

const DonateButton = styled.a`
  color: ${({theme}) => theme === 'light' ? '#24221f' : '#fff' };
  text-decoration: none;
  display: block;
  cursor: pointer;
  background: transparent;
  border: 2px solid ${({theme}) => theme === 'light' ? '#24221f' : '#fff' };
  font-size: 2rem;
  text-transform: uppercase;
  padding: 28px 40px 30px;
  margin-bottom: 40px;
  text-decoration: none;
  position: relative;
  transition: color .2s ease-out;
  & button:hover{
    opacity: .6;
  }

  &:before{
    content: '';
    position: absolute;
    z-index: -1;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    transform: scaleX(0);
    transform-origin: 0 50%;
    transition: transform .2s ease-out;
  }

  ${media.tablet`
  font-size: 30px;
  line-height: 1.2;
  padding: 25px 35px;
  margin-bottom: 36px;
  `}

  ${media.mobile`
  font-size: 28px;
  padding: 25px 30px;
  margin-bottom: 32px;
  `}
`

const ShareSection = styled.div`
  ${container}
  display: flex;
  margin-top: 40px;
  margin-left: auto;
  margin-bottom: 52px;
  font-size: 2rem;

  &:before{
    content: 'Share to';
    margin-right: 32px;
  }

  & rect{
    opacity: 0;
  }

  & button{
    transition: opacity .2s ease-out;
  }

  & button:hover{
    opacity: .6;
  }

  & button:not(:last-of-type){
    margin-right: 20px;
  }

  ${media.tablet`
  margin-top: 20vh;
  margin-bottom: 0;
  margin-left: unset;
  font-size: 30px;

  &:before{
    line-height: 0.8;
  }
  `}

  ${media.mobile`
  font-size: 24px;
  margin-top: 16vh;

  & svg{
    width: 21px;
    height: 21px;
  }

  `}
`

const ExploreMore = styled(Link)`
  color: ${({theme}) => theme === 'light' ? '#24221f' : '#fff' };
  display: block;
  text-transform: uppercase;
  text-decoration: none;
  font-size: 1.5rem;
  font-weight: 500;
  transition: opacity .2s ease-out;

  &:hover{
    opacity: .5;
  }

  ${media.tablet`
  font-size: 22px;
  line-height: 26px;
  `}
`

const LinksContainer = styled.div`
  text-align: center;
`

const ConclusionContent = styled.div`
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: space-evenly;
  height: 100%;
  padding: 25px 0;

  ${media.tablet`
  padding: 0;
  `}

  ${media.mobile`
  padding-bottom: 20px;
  `}
`

const ConclusionContainer = styled(BaseStep)`
  color: ${({theme}) => theme === 'light' ? '#24221f' : '#fff' };

  & ${DonateButton}{
    transition: all 0.4s ease-out;
  }

  & ${DonateButton}:hover{
    color: ${({theme}) => theme === 'light' ? '#fff' : '#24221f' };
    background-color: ${({theme}) => theme === 'dark' ? '#fff' : '#24221f' };
  }

  & ${DonateButton}:hover:before{
    background: ${({theme}) => theme === 'light' ? '#24221f' : '#fff' };
    transform: scaleX(1);
  }

  & ${ShareSection} svg *{
    fill: ${({theme}) => theme === 'light' ? '#24221f' : '#fff' };
  }

  ${media.tablet`
  justify-content: space-around;
  `}
`

const ConclusionStep = ({text, backgroundImage, backgroundColor, number, theme, donationUrl, isLast }) => (
  <ConclusionContainer
    isLast={isLast}
    backgroundImage={backgroundImage}
    backgroundColor={backgroundColor}
    layout
    number={number}
    theme={theme}
  >
    <ShareSection>
      <WhatsappShareButton url={window.location.href}>
        <WhatsappIcon />
      </WhatsappShareButton>
      <FacebookShareButton url={window.location.href}>
        <FacebookIcon />
      </FacebookShareButton>
      <TwitterShareButton url={window.location.href}>
        <TwitterIcon />
      </TwitterShareButton>
      <LinkedinShareButton url={window.location.href}>
        <LinkedinIcon />
      </LinkedinShareButton>
    </ShareSection>
    <ConclusionContent>
      <Text>
        {text}
      </Text>
      <LinksContainer>
        <DonateButton href="https://whc.unesco.org/en/donation/" theme={theme}>
          Donate to the museum
        </DonateButton>
        <ExploreMore to="/" theme={theme}>
          Explore more
        </ExploreMore>
      </LinksContainer>
    </ConclusionContent>
  </ConclusionContainer>
);

export default ConclusionStep;
