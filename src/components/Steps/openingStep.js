import React from 'react';
import styled from 'styled-components';
import { motion } from "framer-motion";
import { media } from "utils";
import BaseStep from './baseStoryStep';

const DateText = styled.div`
  font-family: "Barlow Condensed";
  font-size: 20px;
  font-stretch: condensed;
  line-height: 24px;
  color: #ffffff;
  text-transform: uppercase;
  ${media.tablet`
  font-size: 18px;
  line-height: 22px;
  `}
  ${media.mobile`
  font-size: 17px;
  line-height: 20px;
  `}
`

const MuseumText = styled.div`
  font-family: "Barlow Condensed";
  font-size: 32px;
  font-stretch: condensed;
  line-height: 38px;
  color: #ffffff;
  ${media.tablet`
  font-size: 30px;
  line-height: 36px;
  `}
  ${media.mobile`
  font-size: 28px;
  line-height: 34px;
  `}
`

  const ExpositionTitle = styled(motion.h1)`
  font-family: "Barlow Condensed";
  font-size: 80px;
  font-weight: 500;
  font-stretch: condensed;
  line-height: 96px;
  text-align: center;
  color: #ffffff;
  text-transform: uppercase;
  ${media.tablet`
    font-size: 60px;
    line-height: 72px;
  `}
  ${media.mobile`
    font-size: 42px;
    line-height: 50px;
    width: 70%;
  `}
`;

const Description = styled(motion.p)`
  width: 70%;
  font-family: "Barlow Condensed";
  font-size: 32px;
  font-stretch: condensed;
  line-height: 38px;
  text-align: center;
  color: #ffffff;
  ${media.tablet`
    font-size: 30px;
    line-height: 36px;
    width: 80%;
  `}
  ${media.mobile`
    width: 89%;
    line-height: 25px;
    font-size: 21px;
  `}
`;

const OpeningContainer = styled(BaseStep)`
  & > ${DateText} {
    position: relative;
    margin-top: 5%;
    margin-bottom: 1%;
    ${media.tablet`
    margin-top: 19%;
    `}
    ${media.mobile`
    margin-top: 34%;
    margin-bottom: 2%;
    `}
  }
  & > ${MuseumText} {
    margin-bottom: 8%;
    ${media.tablet`
    margin-bottom: 11%;
    `}
    ${media.mobile`
    margin-bottom: 15.5%;
    `}
  }
  & > ${ExpositionTitle} {
    margin-bottom: 5%;
    ${media.tablet`
    margin-bottom: 8%;
    `}
    ${media.mobile`
    margin-bottom: 10.5%;
    `}
  }

  & > ${Description} {
    margin-bottom: 50px;
  }
`;

const OpeningStep = ({ backgroundImage, created, museum, exposition, description, active, backgroundColor, ...props }) => (
  <OpeningContainer
    backgroundImage={backgroundImage}
    backgroundColor={backgroundColor}
    active={active}
    {...props}
  >
    <DateText>
      {created}
    </DateText>
    <MuseumText>
      {museum}
    </MuseumText>
    <ExpositionTitle>
      {exposition}
    </ExpositionTitle>
    <Description>
      {description}
    </Description>
  </OpeningContainer>
);

export default OpeningStep;
