import React from 'react';
import { motion } from "framer-motion";
import styled from "styled-components";
import propTypes from "prop-types";
import DownArrowIcon from "icons/downArrow";


const DownArrow = styled(DownArrowIcon)`
  position: absolute;
  bottom: 55.8px;
  margin: 0 auto; 
  width: 100%;
  cursor: pointer;
  z-index: 20;
  height: 14.3px;
` 
export const pageHeight = "100vh";
export const pageWidth = "100vw";

const StepContainer = styled.div`
height: ${pageHeight};
width: ${pageWidth};
position: relative;
`;

const FlexContainer = styled(motion.div)`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  height: 100%;
  &::before {
  content: "";
  position: absolute;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;
  background: ${({ backgroundImage, backgroundColor }) => backgroundImage? `url(${backgroundImage}) no-repeat center center` : backgroundColor} ;
  background-size: cover;
  filter: ${ ({backgroundImage}) => backgroundImage && "blur(0.25rem)"};
  overflow: hidden;
  }
  position: relative;
  & > * {
  position: relative;
  }
`;

const BlackGround = styled.div`
  position: absolute;
  width: 100vw;
  height: 100vh;
  left: 0;
  top: 0;
  z-index: -1;
  background: #000;
`

const BaseStep = ({children, isLast, handleArrowClick, theme, ...props}) => (
  <StepContainer>
    <FlexContainer  {...props}>
      {children}
    </FlexContainer>
    {!isLast && <DownArrow onClick={handleArrowClick} theme={theme} />}
    <BlackGround />
  </StepContainer>
)

BaseStep.defaultProps = {
  backgroundColor: "#ffffff",
}

BaseStep.propTypes = {
  backgroundColor: propTypes.string,
  backgroundImage: propTypes.string,
}


export default BaseStep;
