import React from 'react';
import { Link as BaseLink } from "gatsby"
import styled from 'styled-components';
import Logo from 'icons/logo';
import propTypes from "prop-types";
import { media } from "utils";

const StepperContainer = styled.div`
  position: fixed;
  top: 0;
  display:flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 80px;
  height: 100vh;
  z-index: 10;
  background-color: ${ ({ light }) => light ? "rgba(238, 237, 235, 0.33)" : "rgba(36, 34, 31, 0.33)" };
  ${media.tablet`
  height: 60px;
  width: 100vw;
  flex-direction: row;
  justify-content: flex-end;
  `}
  ${media.mobile`
  height: 49px;
  `}

`;

const Link = styled(BaseLink)`
  text-decoration: none;
`

const Step = styled.div`
  width: 16px;
  height: 16px;
  background-color: ${ ({isActive}) => isActive ? "white" : "transparent" };
  box-sizing: border-box;
  border: solid 2px #ffffff;
  border-radius: 16px;
  z-index: 15;
  cursor: pointer;
  transition: background .2s ease-out;

  ${ ({isActive}) => isActive || `
  &:hover{
    background: #8b8c8e;
  }
  `};

  ${media.mobile`
  width: 14px;
  height: 14px;
  `}
`;

const StyledLogo = styled(Logo)`
position: absolute;
top: 32px;
left: 16px;
width: 48px;
height: 30.3px;
z-index: 15;
${media.tablet`
  top: 15px;
  left: 40px;
  `}
${media.mobile`
  left: 20px;
  top: 11px;
  width: 42px;
  height: 26.5px;
`}
`;

const StepsContainer = styled.div`
  & > *:not(:last-child) {
    margin-bottom: 12px;
  }
  ${media.tablet`
    display: flex;
    flex-direction: row;
    align-items: center;
    margin-right: 40px;
  & > *:not(:last-child) {
    margin-bottom: 0px;
    margin-right: 12px;
  }
  `}
  ${media.mobile`
    margin-right: 20px;
  & > *:not(:last-child) {
    margin-right: 11px;
    margin-bottom: 0px;
  }
  `}
`

const Steps = ({ number, active, goToStep }) =>  (
  <StepsContainer>
    {
      [...Array(number)].map((_, i) => (
        <Step onClick={() => goToStep(i)} key={i} isActive={active===i} />
      ))
    }
  </StepsContainer>
);

const Stepper = ({ number, active, goToStep }) => (
  <StepperContainer>
    <Link to="/">
      <StyledLogo />
    </Link>
    <Steps number={number} active={active} goToStep={goToStep} />
  </StepperContainer>
);

Stepper.defaultProps = {
  number: 6,
  active: 0,
}

Stepper.propTypes = {
  number: propTypes.number,
  active: propTypes.number,
}

export default Stepper;
