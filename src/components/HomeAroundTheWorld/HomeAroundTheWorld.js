import React, { useMemo, useState } from 'react';
import styled from "styled-components";
import { container, media } from "utils"

import Flicking from "@egjs/react-flicking";
import { AutoPlay } from "@egjs/flicking-plugins";

import img1 from "images/img1.jpg"
import img2 from "images/img2.jpg"
import img3 from "images/img3.jpg"
import img4 from "images/img4.jpg"
import img5 from "images/img5.jpg"
import img6 from "images/img6.jpg"
import img7 from "images/img7.jpg"
import img8 from "images/img8.jpg"
import img9 from "images/img9.jpg"
import img10 from "images/img10.jpg"
import img11 from "images/img11.jpg"
import img12 from "images/img12.jpg"
import img13 from "images/img13.jpg"
import img14 from "images/img14.jpg"
import img15 from "images/img15.jpg"
import img16 from "images/img16.jpg"
import img17 from "images/img17.jpg"
import img18 from "images/img18.jpg"

const expositionsLists = [
    {
	collection: 'Asia',
	items: [
	    {
		name: 'Great Wave off Kanagawa',
		img: img1,
		museum: 'British Museum '
	    },
	    {
		name: `A day's work`,
		img: img2,
		museum: 'Work History Museum'
	    },
	    {
		name: 'The kiss',
		img: img3,
		museum: 'MOMA'
	    },
	    {
		name: `Tangling excitement`,
		img: img4,
		museum: 'Paris National Museum'
	    },
	    {
		name: `Athena's song`,
		img: img5,
		museum: 'Museum of Illusions'
	    },
	    {
		name: 'Loneliness bite',
		img: img6,
		museum: 'Street Museum'
	    },
	    {
		name: `Ming's legacy`,
		img: img7,
		museum: `Taiwan's Artifact Museum`
	    },
	    {
		name: 'Real sized T-Rex',
		img: img8,
		museum: `Mexico's Science Museum`
	    },
	    {
		name: 'Grief and Sorrow',
		img: img9,
		museum: 'Pantheon Museum'
	    }
	]
    },
    {
	collection: 'Europe',
	items: [
	    {
		name: 'Eldorado 1500',
		img: img10,
		museum: 'Louvre'
	    },
	    {
		name: 'Dali, Dali and Dali',
		img: img11,
		museum: 'El Greco Museum'
	    },
	    {
		name: 'Mirror',
		img: img12,
		museum: 'Irish Museum of Art'
	    },
	    {
		name: 'Beautiful Fields Forever',
		img: img13,
		museum: 'Kelvingrove Art Gallery'
	    },
	    {
		name: 'Object Life',
		img: img14,
		museum: 'Verdant Works'
	    },
	    {
		name: 'Polynesian Artifacts',
		img: img15,
		museum: 'Newseum'
	    },
	    {
		name: 'Standing Up',
		img: img16,
		museum: 'The Alamo'
	    },
	    {
		name: 'Irish Tradition',
		img: img17,
		museum: 'Hirshhorn Museum'
	    },
	    {
		name: 'The Celtic Life',
		img: img18,
		museum: 'Fountainhead Antique'
	    }
	]
    },
]

const StyledSection = styled.section`
	background-color: #24221f;
	padding: 56px 0 120px;
	position: relative;

	${media.tablet`
	padding-top: 37px;
	`}

	${media.mobile`
	padding-top: 35px;
	padding-bottom: 78px;
	`}
`

const ExpositionMuseum = styled.div`
	color: #fff;
	font-size: 24px;
`

const ExpositionName = styled.h3`
	font-size: 40px;
	font-weight: 500;
	color: #fff;
`

const ExpositionInfo = styled.div`
	transform: skew(10deg);
	text-align: center;
`

const ExpositionInfoContainer = styled.div`
	position: absolute;
	display: flex;
	justify-content: center;
	align-items: center;
	left: 50%;
	transform: translateX(-50%);
	background-color: #00000088;
	width: 100%;
	height: 100%;
	opacity: 0;
	transition: opacity .2s ease-out;

	& ${ExpositionName}{
		margin-bottom: 12px;
	}
`

const ExpositionImage = styled.img`
	position: absolute;
	left: 50%;
	top: 0;
	width: 150%;
	height: 100%;
	object-fit: cover;
	will-change: transform;
	transform: translateX(-50%) skew(10deg);
	transition: filter .2s ease-out;
`

const BlackGround = styled.div`
	position: absolute;
	width: 100%;
	height: 100%;
	left: 0;
	top: 0;
	z-index: -1;
	background: #000;
`

const ExpositionInnerContainer = styled.div`
	width: 100%;
	height: 100%;
	transform: translateX(-50%) skew(-10deg);
	overflow: hidden;
	position: absolute;
	left: 50%;
	transition: width .2s ease-out;
`

const ExpositionContainer = styled.div`
	width: 350px;
	height: 500px;

	&:hover{
		z-index: 5;
	}

	&:hover ${ExpositionInnerContainer}{
		width: 120%;
	}

	&:hover ${ExpositionImage}{
		filter: blur(3px);
	}

	&:hover ${ExpositionInfoContainer}{
		opacity: 1;
	}

	${media.tablet`
	&:nth-child(2){
		z-index: 5;
	}

	&:nth-child(2) ${ExpositionInnerContainer}{
		width: 120%;
	}

	&:nth-child(2) ${ExpositionImage}{
		filter: blur(3px);
	}

	&:nth-child(2) ${ExpositionInfoContainer}{
		opacity: 1;
	}
	`}

	${media.mobile`
	width: 250px;
	height: 412px;
	`}
`

const SectionHeader = styled.div`
	${container}
	display: flex;
	justify-content: space-between;
	align-items: center;
	margin-bottom: 64px;

	${media.mobile`
	margin-bottom: 42px;
	`}
`

const ExpositionSelector = styled.div`
	font-size: 64px;
	color: #fff;
	font-weight: 500;
	cursor: pointer;
	position: relative;

	&:after{
		content: '';
		width: 0;
		height: 0;
		border-top: 16px solid #9d9b9a;
		border-left: 14px solid transparent;
		border-right: 14px solid transparent;
		display: inline-block;
		margin-left: 48px;
		margin-bottom: 7px;
	}

	${media.tablet`
	font-size: 45px;

	&:after{
		margin-left: 30px;
		margin-bottom: 5px;
		border-top-width: 12px;
		border-left-width: 10.5px;
		border-right-width: 10.5px;
	}
	`}

	${media.mobile`
	font-size: 35px;

	&:after{
		border-top-width: 9px;
		border-left-width: 8px;
		border-right-width: 8px;
		margin-left: 28px;
		margin-bottom: 4px;
	}
	`}
`

const ExpositionOption = styled.div`
	color: #24221f;
	font-size: 32px;
	font-weight: 400;
	opacity: ${({selected}) => selected ? '.33' : '1'};
	pointer-events: ${({selected}) => selected ? 'none' : 'all'};
	cursor: ${({selected}) => selected ? 'auto' : 'pointer'};

	${media.tablet`
	font-size: 30px;
	`}
`

const ExpositionOptions = styled.div`
	background-color: #fff;
	opacity: 0.9;
	padding: 32px 40px;
	max-width: 400px;
	position: absolute;
	z-index: 2005;
	top: calc(100% + 24px);
	right: 0;
	pointer-events: none;

	& ${ExpositionOption}{
		margin-bottom: 16px;
	}

	${media.tablet`
	padding: 28px 35px;
	`}

	${media.mobile`
	top: calc(100% + 21px);
	padding: 28px 32px;

	& ${ExpositionOption}{
		margin-bottom: 14px;
	}
	`}
`

const SectionLabel = styled.h2`
	${container}
	font-size: 152px;
	font-weight: 500;
	color: #eeedeb;
	position: absolute;
	text-transform: uppercase;
	opacity: .5;
	right: 0;
	bottom: calc(100% - 33px);

	&:after{
		content: '';
		position: absolute;
		width: 100%;
		height: 33px;
		right: 0;
		bottom: 0;
		background: #24221f;
	}

	${media.tablet`
	font-size: 120px;
	`}

	${media.mobile`
	font-size: 70px;
	bottom: calc(100% - 14px);

	&:after{
		height: 14px;
	}
	`}
`

const NavigationTip = styled.div`
	color: #9d9b9a;
	font-size: 32px;
	font-weight: 500;
	
	&:before{
		content: 'Click and Drag ';
		color: #fff;
	}

	${media.tablet`
	font-size: 30px;

	&:before{
		content: 'Swipe '
	}
	`}

	${media.mobile`
	display: none;
	`}
`

const plugins = [
	new AutoPlay({
    duration: 2000,
    direction: "NEXT",
    stopOnHover: true,
}),
];

const mapExpositions = (expositions) => expositions.items.map((exposition, index) => (
	<ExpositionContainer key={index}>
		<ExpositionInnerContainer>
			<BlackGround />
			<ExpositionImage src={exposition.img}/>
			<ExpositionInfoContainer>
				<ExpositionInfo>
					<ExpositionName>
						{exposition.name}
					</ExpositionName>
					<ExpositionMuseum>
						{exposition.museum}
					</ExpositionMuseum>
				</ExpositionInfo>
			</ExpositionInfoContainer>
		</ExpositionInnerContainer>
	</ExpositionContainer>
))

const HomeAroundTheWorld = () => {
	const [selectedExpos, setSelectedExpos] = useState(expositionsLists[0]);
	const [openedOptions, setOpenedOptions] = useState(false);
	const expos = useMemo( () => mapExpositions(selectedExpos), [selectedExpos]);

	return (
		<StyledSection>
			<SectionLabel>Discover</SectionLabel>
			<SectionHeader>
				<ExpositionSelector onClick={() => setOpenedOptions(!openedOptions)}>
					From {selectedExpos.collection}
					{openedOptions &&
						<ExpositionOptions>
							{expositionsLists.map((list, index) =>
								<ExpositionOption
									key={index}
									selected={index === expositionsLists.indexOf(selectedExpos)}
									onClick={() => setSelectedExpos(expositionsLists[index])}>{list.collection}
								</ExpositionOption>
							)}
						</ExpositionOptions>
					}
				</ExpositionSelector>
				<NavigationTip>
					to navigate
				</NavigationTip>
			</SectionHeader>
				<Flicking 
					gap={0} 
					circular 
					plugins={plugins}
					isConstantSize
					isEqualSize
					renderOnlyVisible
					duration={1800} 
					panelEffect={(x => x)} 
					collectStatistics={false}>
					{expos}
				</Flicking>
		</StyledSection>
	)
}

export default HomeAroundTheWorld


