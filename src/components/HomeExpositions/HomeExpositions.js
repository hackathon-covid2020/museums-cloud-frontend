import React from 'react';
import styled from "styled-components";
import { media } from "utils";

const StyledSection = styled.section`
    background-color: #fff;
    position: relative;
`

const SectionDecoration = styled.div`
    position: absolute;
    bottom: 100%;
    right: 0;
    width: 0;
    height: 0;
    border-top: 120px solid transparent;
    border-right: 50vw solid #fff;
    border-left: 50vw solid #fff;
    border-bottom: 0;

    ${media.mobile`
    border-top-width: 60px;
    `}

`

const HomeExpositions = () => (
    <StyledSection>
	<SectionDecoration />
    </StyledSection>
)

export default HomeExpositions
