import React, { useEffect, useState } from "react"
import axios from "axios";
import { useLocation } from '@reach/router';

import Layout from "components/layout"
import SEO from "components/seo"
import Exposition from "components/ExpositionManager"

const exhbitionsApi = "https://ladoescurodalua.us-south.cf.appdomain.cloud/api/exhibitions"

const buildSteps = (exposition) => exposition.catalogue.reduce( (acc, item, index) => {

  if(index === 0){
    return [
      {
        type: "opening",
        created: exposition.startDate,
        museum: exposition.museumName,
        exposition: exposition.title,
        description: item.informationBit,
        backgroundImage: item.backgroundImage,
        backgroundColor: item.backgroundColor,
        theme: item.theme,
      }
    ]
  }

  if(index === exposition.catalogue.length - 1){
    return [
      ...acc,
      {
        type: "conclusion",
        donationUrl: item.donationUrl,
        backgroundImage: item.backgroundImage,
        backgroundColor: item.backgroundColor,
        theme: item.theme,
        text: item.informationBit
      }
    ]
  }

  if( index % 2 === 0 ){
    return [
      ...acc,
      {
        type: "leftAlignedPiece",
        title: item.title,
        author: item.author,
        year: item.productionYear,
        description: item.informationBit,
        art: item.imageUrl,
        backgroundColor: item.backgroundColor,
        backgroundImage: item.backgroundImage,
        theme: item.theme
      }
    ]
  }

  if( index % 1 === 0 ){
    return [
      ...acc,
      {
        type: "rightAlignedPiece",
        title: item.title,
        author: item.author,
        year: item.productionYear,
        description: item.informationBit,
        art: item.imageUrl,
        backgroundColor: item.backgroundColor,
        backgroundImage: item.backgroundImage,
        theme: item.theme
      }
    ]
  }

  return []

}, [])

const getExhibitionId = pathname => pathname.split('/').filter(piece => piece.length).pop()

const IndexPage = () => {
  const location = useLocation();
  const [expo, setExpo] = useState(null);

  useEffect(() => {
    const exhibitionId = getExhibitionId(location.pathname)

    axios.get(`${exhbitionsApi}/${exhibitionId}`)
      .then(resultData => {
        setExpo(buildSteps(resultData.data.details))
      })
      .catch( err => console.log(err))
  }, [location.pathname])

  return expo ?  (
    <Layout hideNav hideFooter>
      <SEO title="Home" />
      <Exposition steps={expo} />
    </Layout>
  ) : "Loading..." 
}

export default IndexPage
