import React from "react"
import Layout from "components/layout"
import SEO from "components/seo"

import HomeHero from "components/HomeHero"
import HomeExpositions from "components/HomeExpositions"
import HomeAroundTheWorld from "components/HomeAroundTheWorld"
import Highlights from "components/Highlights"

import img1 from "images/birmingham.png"
import img2 from "images/museums-trust.png"
import img3 from "images/UFSCar.png"
import img4 from "images/beauty.png"
import img5 from "images/mr-tt-xb0wLfZH9Zo-unsplash.png"
import img9 from "images/birmingham-museums-trust-hEPmVFvF3Hs-unsplash.png"

const mockedHighlights = [
	{
		date: + new Date(2020, 4, 26),
		image: img1,
		exposition: "Birmingham's Past",
		link : "/exhibition/5df22709-ebf9-4277-9da7-ec2af16d4ffe",
		museum: "Tate Britain"	
	},
	{
		date: + new Date(2019, 6, 20),
		image: img2,
		exposition: "The Wind Rises",
		museum: "Royal Air Force Museum"	
	},
	{
		date: + new Date(2020, 3, 20),
		image: img3,
		exposition: "Being a Student in 1970",
		museum: "UFSCar University"
	},
	{
		date: + new Date(2019, 3, 20),
		image: img4,
		exposition: "Whispers of the Heart",
		museum: "Dublin Castle"
	},
	{
		date: + new Date(2020, 5, 20),
		image: img5,
		exposition: "Picasso and his Dreams",
		museum: "Marly's Museum"
	},
	{
		date: + new Date(2020, 3, 20),
		image: img9,
		exposition: "Dinosaurs",
		museum: "Edinburgh's National Museum"
	}
]

const IndexPage = () => (
	<Layout hideNav>
		<SEO title="Home" />
		<HomeHero />
		<HomeExpositions />
		<Highlights highlights={mockedHighlights}/>
		<HomeAroundTheWorld />
	</Layout>
);

export default IndexPage
