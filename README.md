# Museums Cloud Frontend
Museums Cloud Website, implemented using [GatsbyJS](https://www.gatsbyjs.org/).

[Click here](https://museums.cloud) to see it in action.

# Developing locally

Navigate into directory and start it up.

```shell
gatsby develop
```
Now the site is running at `http://localhost:8000`!

# Deploying
1. Install  wrangler:
```
npm i @cloudflare/wrangler -g
```
2. Authenticate using:
```
wrangler config
```
3. On the project root: 
```
wrangler init --site museum
```

4. Create the file wrangler.toml with Cloudflare configuration

```toml
name = "museums"
type = "webpack"
account_id = ""
workers_dev = true

[env.production]
zone_id = ""
route = "museums.cloud/*"

[site]
bucket = "./public"
entry-point = "workers-site"
```

5. Deploy it:
```sh
gatsby build && wrangler publish --env production
```
