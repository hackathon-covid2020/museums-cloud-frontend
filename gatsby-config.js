module.exports = {
  siteMetadata: {
    title: `Museums Cloud`,
    description: `Museums Cloud - Wonder the beauty`,
    author: `Musems Cloud Team`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: `gatsby-plugin-sharp`,
      options: {
        useMozJpeg: true,
        stripMetadata: true,
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: "MuseumsCloud",
        short_name: "MuseumsCloud",
        start_url: `/`,
        background_color: `#24221f`,
        theme_color: `#24221f`,
        display: "standalone",
        icon: `src/images/logo.svg`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-plugin-prefetch-google-fonts`,
      options: {
        fonts: [
          {
            family: `Barlow Condensed`,
            variants: [`300`, `400`, `500`, `600`, `700`]
          },
        ],
      },
    },
  `gatsby-plugin-styled-components`,
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
